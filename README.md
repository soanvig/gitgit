**DISCLAIMER**

This project is stop, but ready to be maintained again.

I decided to stop the development process, because of enormous amount of work,
that needs to be done, to make this tool considerably more *complete*.

Basic git-things are done, but the time has come to implement merging, resolving
conflicts, drawing `git-log` tree and so on. This is incredibly time-consuming,
and while by myself I'm using `git-cli` it makes no sense to develop that tool just for me.

---

# GitGit

![version v0.2.0](https://img.shields.io/badge/version-v0.2.0-blue.svg)

**GitGit** an acronym for _Gui IT Git_. It is Git tool utilising GUI (_graphical user interface_).

**GitGit** is an app written in [Electron](https://electronjs.org/), free to use both commercial and personal.

![screenshot](./screenshot.png)

This repository is monorepo, so under [packages](./packages) dir, you will find everything related to the app.

## TOC

<!-- TOC -->

- [GitGit](#gitgit)
  - [TOC](#toc)
  - [Supports](#supports)
  - [Notes](#notes)
  - [Changelog](#changelog)
  - [License](#license)
  - [Development](#development)
    - [Setup](#setup)
  - [Packages](#packages)

<!-- /TOC -->

## Supports

- Adding/removing files from commit
- Committing changes with title and description
- Pushing changes alongside with credentials support and creating upstream
- Checkouting existing branches
- Pulling changes
- Branch creating
- Git credentials store

Whole history of features can be found in [changelog features](./CHANGELOG).

## Notes

It is advisible to set `git config push.default upstream` in repository.

Version 0.2.0 is development only version. To use it see [#Development](#development).

## Changelog

[Changelog](./CHANGELOG)

## License

[License GNU GPLv3](./LICENSE)

## Development

### Setup

Because GitGit utilises [Yarn Workspaces](https://yarnpkg.com/blog/2017/08/02/introducing-workspaces/)
it is neccessary to install [Yarn](https://yarnpkg.com/).

1. Clone parent repository:
  `git clone https://gitlab.com/soanvig/gitgit`
2. Enter repository:
  `cd gitgit`
3. Install packages:
  `yarn install`
4. Enter core:
  `cd packages/gitgit-core`
5. Launch backend:
  `yarn backend`
6. Launch frontend:
  `yarn frontend`

## Packages

- [GitGit Core](./packages/gitgit-core)
- [GitGit Runner](./packages/gitgit-runner)
- [GitGit Watcher](./packages/gitgit-watcher)
- [GitGit VStore](./packages/gitgit-vstore)
