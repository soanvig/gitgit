# Contributing

To begin with: thank you for contributing.

When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

## First contribution

### Issues

The major part of the contribution revolvs around [issues](https://gitlab.com/soanvig/gitgit/issues). Issues can be created, discussed and fixed or closed by merge request (MR, GitLab's alias for pull requests). You don't even know how to code!

Issue can be:

- a bug report
- feature request
- improvement request
- help request
- discussion

For each of these category appropriate issue label is created, so make sure you use it!

**Remember**: if you report a bug don't forget about reproducing steps, if you need help - describe what you want to achieve as much detailed as possible.

### Merge requests

If you want to help creating GitGit, and you know how to code in JavaScript, TypeScript or HTML/CSS duet
you may contribute directly to the ecosystem of GitGit.

For first contributions you may want to look for "First contribution" label, which marks issues/tasks, that can be fairly easily taken by people who are new to GitGit or coding.

## Merge request process

1. Start MR if associated issue exists. If not - create it first.
2. Add *In progress* label to issue, or ask issue creator of repo owner to set it for you.
3. Fork repository, pull **development** branch, create new branch out of it (preffered name: `feature/[issue-id]-slug-name` e.g. `feature/1-nice-feature). NOTE: the bugfix may be marked as feature branch too.
4. Create MR and mark it with WIP (*Work in progress*)
  title prefix (this function is natively supported by GitLab). Reference the issue in MR via hash tag + issue id (e.g. #1). Choose MR template which fits your needs.
5. Apply changes.
6. Ensure TypeScript typings are correct, TSLint/ESLint returns no errors, tests passes.
7. Push changes, and see if CI works.
8. Remove WIP status from your MR.
9. Wait for repo owner to merge your MR.

Your MR may not be accepted (it means, it will be returned until it is corrected) or it may become completely rejected, if referenced issue becomes rejected too.