const uniqid = require('uniqid')

/**
 * Timeout in ms between each queue execution
 */
let timeout = 150

/**
 * Stores information in queue
 *
 * @param {string} key - key
 * @param {object} value - value
 * @property {Function} value.func - function to be executed
 * @property {*} value.context - context for function
 * @property {Array.<*>} value.args - array of arguments for function
 */
const queue = new Map()

/**
 * Holds state of runner.
 */
let isRunning = false

/**
 * Adds items to queue, and launches runner, if it's not already running.
 *
 * @param {string} key - key of the queue item
 * @param {Function} func - function, which should be executed
 * @param {object} options - options object
 * @property {*} [context=null] - context, in which function should be executed
 * @property {Array.<*>} [args=[]] - array of arguments, that should be passed to the function
 * @property {boolean} [force=false] - determines, whether item should be added to queue,
 *  even if element with that key exist. If `true`, the key will receive uniqid, and will be added.
 */
function add (key, func, {
  context = null,
  args = [],
  force = false,
}) {
  // If key exists in queue, and it's not force-adding, don't add
  if (queue.has(key) && !force) {
    return false
  }

  // If we are force adding, add uniqid
  if (force) {
    key = key + uniqid()
  }

  // Add to queue
  queue.set(key, {
    func,
    context,
    args,
  })

  if (!isRunning) {
    run()
  }

  return true
}

/**
 * Executes queue every 200ms if there are still some items.
 */
function run () {
  if (queue.size > 0) {
    isRunning = true

    queue.forEach(({ func, context, args }, key) => {
      func.apply(context, args)
    })

    queue.clear()

    setTimeout(run, timeout)
  } else {
    isRunning = false
  }
}

module.exports = {
  queue,
  isRunning,
  add,
  run,

  /**
   * Sets new timeout for runner execution
   *
   * @param {number} time - new timeout in ms
   */
  set timeout (value) {
    timeout = time
  },

  get timeout () {
    return timeout
  },
}
