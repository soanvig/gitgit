# GitGit Runner

Queues actions (by unique key), and launches queue every (default) 150ms.

## Usage

```js
const runner = require('gitgit-runner')

function someFunc (a, b) {
 return a + b
}

// Configure our timeout
runner.timeout = 100

// Add function of key "myFunc"
runner.add('myFunc', someFunc, {
  context: global, // 'this' in function will point global
  args: [1, 2], // arguments array, a = 1, b =2
  force: false, // default value, force adds item to queue
})

// runner will launch immediately, and clear the queue
// also runner will **enqueue next launch** for timeout

// Add again - it will add because queue was cleared
runner.add('myFunc', someFunc, {
  args: [1, 2],
})

// Add again - it won't add, because runner didn't launched.
// It is waiting for timeout
runner.add('myFunc', someFunc)

// But if one really wants to add that function once again
// one can set 'force: true'
runner.add('myFunc', someFunc, {
  args: [1, 2], // arguments array, a = 1, b =2
  force: true, // force adds item to queue
})

// Now timeout passes and runner executes all functions in queue (first and third)
// After clearing queue it will wait timeout again, and make new attempt to clear queue
// if queue is empty, it will stop execution and wait for new item in queue.
// After adding that item, runner will launch immediately again.
```