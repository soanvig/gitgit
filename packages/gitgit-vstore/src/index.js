function VStore (state = {}) {
    const Vue = VStore._vue

    if (!Vue) {
      throw 'VStore needs to be installed before it can be instantiated.'
    }

    // _vm Vuex implementation
    this._vm = new Vue({
      data: {
        $$state: state,
      },
    })

    this.state = state
}

VStore.install = (Vue) => {
  // If already installed
  if (VStore._vue) {
    return
  }

  VStore._vue = Vue;
}

VStore.init = (klass, state = {}) => {
  const Vue = VStore._vue
  const instance = new klass(state)
  Vue.prototype.$store = instance
  VStore.root = instance

  return instance
}

VStore.module = (klass, name, state = {}) => {
  const Vue = VStore._vue

  if (Vue.prototype.$store[name]) {
    throw `The module's name: "${name}" is already occupied on root store.`
  }

  const instance = new klass(state)

  // Save it on global store
  Vue.prototype.$store[name] = instance

  // Save reference to root
  instance.root =  Vue.prototype.$store

  return instance
}

VStore.root = null

module.exports = VStore
