# GitGit Server

Creates insecure local git server, which may be accessed from localhost or LAN.

**WARNING**: this tool exposes given path (repository) to network, which may be dangerous,
although it does not exposes paths above given path.

## Usage

1. Setup server (integrated with node process)

```js
const server = require('gitgit-server')

// Path to single repo or directory with multiple repos
// It's good idea to provide absolute path
server.start('/home/user/git-repositories')

// After some time you may want to stop server
server.stop()
```

2. Connect to server (i.e. clone repository from hosting device: `/home/user/git-repositories/foobar/.git`)

  `git clone git://127.0.0.1/foobar`