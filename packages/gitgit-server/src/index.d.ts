declare module 'gitgit-server' {
  export interface GitServer {
    start;
    stop;
  }

  export function stop (): void;
  export function start (path: string): GitServer;
}