const spawn = require('child_process').spawn

const server = {
  _process: null,

  start (path) {
    if (this._process) {
      throw new Error('Git server process already created. Stop the previous one with .stop()')
    }

    this._process = spawn('git', [
      'daemon',
      '--verbose',
      '--export-all',
      '--reuseaddr',
      `--base-path=${path}`,
      path,
    ])

    return this
  },

  stop () {
    if (this._process) {
      this._process.kill('SIGTERM')
      this._process = null
    }

    return true
  }
}

// Ensure server quits
const signals = ['exit', 'SIGINT', 'SIGUSR1', 'SIGUSR2', 'uncaughtException', 'SIGTERM']
signals.forEach((eventType) => {
  process.on(eventType, () => {
    server.stop()
  })
})

module.exports = server
