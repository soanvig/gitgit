const gitignore = require('gitignore-parser')
const fs = require('fs')
const p = require('path')
const chokidar = require('chokidar')

class Watcher {
  constructor (dir) {
    this.dir = dir
    this.gitignore = this.parseGitignore(dir)
    this.watchHandler = null
  }

  /**
   * Enables watching
   * Returns this
   * @param {Function} cb callback(filename, event) called when file changes
   */
  watch (cb) {
    this.watchHandler = chokidar.watch(this.dir, {
      ignored: (path) => {
        return this.watchFilter(path)
      },
      ignoreInitial: true,
    })

    this.watchHandler.on('all', (event, filename) => {
      cb(filename, event)
    })

    return this
  }

  /**
   * Shuts down watcher
   */
  close () {
    this.watchHandler.close()
    this.watchHandler = null
  }

  /* Private */

  /**
   * Dir where .gitignore file should be looked for
   * Compiles with gitignore package if found
   * @param {String} dir
   */
  parseGitignore (dir) {
    const gitignorePath = p.join(dir, '.gitignore')

    if (!fs.existsSync(gitignorePath)) {
      return null
    }

    return gitignore.compile(fs.readFileSync(gitignorePath, 'utf8'))
  }

  /**
   * Returns true if path should be filtered out
   * False if should be watched
   * @param {String} path absolute path to test ignore
   */
  watchFilter (path) {
    const relativePath = p.normalize(p.relative(this.dir, path))

    // Reject default files if no gitignore
    if (!this.gitignore) {
      // For each path in ignore test, if path starts with it
      return Watcher.IGNORE.some((ignored) => relativePath.indexOf(ignored) === 0)
    }

    // Filter via gitignore
    return !this.gitignore.accepts(relativePath)
  }
}

Watcher.IGNORE = [
  'node_modules',
  '.git',
]

module.exports = Watcher
