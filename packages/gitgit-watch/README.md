# GitGit Watcher

Watches for changes in files in local git repository, and monits given callback.

Doesn't trigger event when changed file matches `.gitignore` rules.

## Usage

```js
// import lib
const Watcher = require('gitgit-watch')

// initialize watcher
const repoDirectory = './some-repo'
const watcher = new Watcher(repoDirectory)

// start watching and pass callback
watcher.watch((filename, event) => {
  console.log('Changed file path: ' + filename)
  console.log('Changed file event: ' + event)

  // For more info about event see:
  // https://nodejs.org/docs/latest/api/fs.html#fs_fs_watch_filename_options_listener
})
```