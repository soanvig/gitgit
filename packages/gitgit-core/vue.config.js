const join = require('path').join

module.exports = {
  outputDir: 'build/renderer',
  configureWebpack: {
    entry: './src/renderer/main.js',
    externals: {
      nodegit: 'nodegit',
    },
    target: 'electron-renderer',
    resolve: {
      alias: {
        '@': join(__dirname, 'src/renderer'),
      },
    },
  },
  devServer: {
    port: 9999,
  },
}
