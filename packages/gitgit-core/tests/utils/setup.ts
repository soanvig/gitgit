import * as fs from 'fs-extra'
import * as path from 'path'
import * as unzip from 'extract-zip'
import * as uuid from 'uuid'
import { exec } from 'child_process'

export async function setup (env: string): Promise<string> {
  const envPath: string = path.join(__dirname, '..', 'envs', env + '.zip')
  const repoPath: string = path.join(__dirname, '..', 'temp', uuid.v4())

  await new Promise((resolve, reject) => {
    fs.ensureDir(repoPath)

    unzip(envPath, { dir: repoPath, defaultDirMode: 511, defaultFileMode: 511 }, function (err) {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })

  return repoPath
}

export async function teardown (repoPath: string): Promise<void> {
  exec(`rm -rf ${repoPath}`)
}
