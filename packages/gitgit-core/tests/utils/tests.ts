import * as ava from 'ava'
import * as sinon from 'sinon'
import { setup, teardown } from './setup'
import { Git } from '../../src/main/git/Git'

interface ITestContext {
  git: Git
  repoPath: string
}

Git.prototype.ipcMain = {
  once: () => { /**/ },
}

const dummyEvent = {
  sender: {
    send: () => { /**/ },
  },
}

export function createTest (
  env: string,
  spies: Array<string> = []
): ava.TestInterface<ITestContext> {
  const test: ava.TestInterface<ITestContext> = ava.test

  test.beforeEach('Preparation', async (t) => {
    const repoPath: string = await setup(env)
    const git = new Git()

    await git.action(dummyEvent, 'init', repoPath)

    spies.forEach((propName) => {
      git[propName] = sinon.spy()
    })

    t.context = {
      git,
      repoPath,
    }
  })

  test.afterEach.always('Cleanup', async (t) => {
    await teardown(t.context.repoPath)
  })

  return test
}

export function someTime (time: number = 1000): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(() => { resolve() }, time)
  })
}
