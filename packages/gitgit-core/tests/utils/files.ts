import * as fs from 'fs-extra'
import * as path from 'path'

export async function createFile (
  repo: string, name: string, content: string = ''
): Promise<string> {
  const filename: string = path.join(repo, name)

  // Creates file if not exists, and cleans it up, if exists ('w' flag)
  // Creates all parent directories (by outputFile())
  await fs.outputFile(filename, content)

  return filename
}
