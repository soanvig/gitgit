import errorLang from './error.json'
import generalLang from './general.json'
import interfaceLang from './interface.json'
import repositoryLang from './repository.json'

export default {
  error: errorLang,
  general: generalLang,
  interface: interfaceLang,
  repository: repositoryLang,
}
