import VStore from 'gitgit-vstore'
import Watcher from 'gitgit-watch'
import Queue from 'gitgit-runner'
import { cloneDeep, debounce } from 'lodash'
import { ipcRenderer } from 'electron'
import { existsSync } from 'fs'

/**
 * File parsed from `git status`
 * @typedef {object} File
 * @property {string} path - path to file, relative to repository
 * @property {string} type - file type in terms of git status: 'created', 'deleted', 'modified', 'renamed'
 */

ipcRenderer.on('store', (event, action, ...args) => {
  VStore.root.repository[action](...args)
})

/**
 * Initial state, used for cloning into other objects.
 * Always clone.
 */
const initialState = {
  /**
   * Selected branch.
   * @type {Reference|null}
   */
  currentBranch: null,

  /**
   * All files in repository.
   * @type {object}
   * @property {Array.<File>} unstaged - files not staged for commit
   * @property {Array.<File>} staged - files staged for commit
   */
  files: {
    unstaged: [],
    staged: [],
  },

  /**
   * repository handler.
   * @type{(null | Repository)}
   */
  repo: null,

  /**
   * Path to repository.
   * @type{string}
   */
  path: '',

  /**
   * All available references.
   * @type {object}
   * @property {Array.<Reference>} head - local references
   * @property {Array.<Reference>} remote - remote references
   */
  references: {
    head: [],
    remote: [],
  },

  /**
   * Watcher object.
   * @type{(null | Watcher)}
   */
  watcher: null,
}

class Repository extends VStore {
  clear () {
    if (this.state.watcher) {
      this.state.watcher.close()
    }

    const stateClone = cloneDeep(initialState)
    for (const stateKey in stateClone) {
      this.state[stateKey] = stateClone[stateKey]
    }
  }

  /**
   * Queues actions on git.js.
   *
   * @async
   * @param {string} action - action to execute
   */
  git (action, ...args) {
    Queue.add(
      'git',
      ipcRenderer.send,
      {
        args: ['git', action, ...args],
      },
    )
  }

  loadRepo (path) {
    if (!path || !existsSync(path)) {
      return
    }

    this.clear()
    this.state.path = path
    this.subscribeWatcher()
    this.git('init', path)
  }

  /**
   * Propagates action to root store
   */
  notify (payload) {
    this.root.notify(payload)
  }

  /**
   * Propagates prompt to root store
   */
  prompt (type, ...args) {
    this.root.prompt[type](...args)
  }

  /**
   * Saves current branch
   *
   * @param {Reference} currentBranch - current branch
   */
  saveCurrentBranch (currentBranch) {
    this.state.currentBranch = currentBranch
  }

  /**
   * Saves files
   *
   * @param {Array.<File>} files - array of files
   */
  saveFiles (files) {
    this.state.files = files
  }

  /**
   * Saves passed references
   *
   * @param {Array.<Reference>} head - array of heads
   * @param {Array.<Reference>} remote - array of remotes
   */
  saveReferences (head, remote) {
    this.state.references.head = head
    this.state.references.remote = remote
  }

  /**
   * Creates new watcher for repository's path, and enables it.
   */
  subscribeWatcher () {
    const watcher = new Watcher(this.state.path)

    // Debounce function to limit multiple calls like removing batch of files
    const cb = (filename, event) => {
      this.watcherNotify({ filename, event })
    }

    watcher.watch(debounce(cb, 100, {
      leading: true,
      trailing: false,
    }))
  }

  /**
   * Dispatches `parseStatus` action on given `payload.status`.
   *
   * @param {object} payload - payload
   * @property {string} payload.filename - name of changed file
   * @property {string} payload.event - event which triggered watcher
   */
  watcherNotify (payload) {
    this.git('status')
  }
}

export default {
  state: initialState,
  klass: Repository,
}
