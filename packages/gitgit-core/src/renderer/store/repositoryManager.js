import VStore from 'gitgit-vstore'

import { remote } from 'electron'
const db = remote.getGlobal('db')

/**
 * Repository object, storing information about repository
 * @typedef {object} Repo
 * @property {number} id - id
 * @property {string} path - path to repository
 */

/**
 * Initial state, used for cloning into other objects.
 * Always clone.
 */
const initialState = {
  /**
   * Array of repos
   * @type {Array.<Repo>}
   */
  repos: [],
}

class RepositoryManager extends VStore {
  /**
   * Action called on repository manager mount.
   */
  init () {
    this.getRepos()
  }

  /**
   * Commits repos from database into store.
   */
  getRepos () {
    this.state.repos = db.get('repositories').value()
  }

  /**
   * Removes repository of given id from database
   *
   * @param {object} payload - payload
   * @property {number} payload.id - id of repo to remove
   */
  remove (payload) {
    db.get('repositories').remove({ id: payload.id }).write()
    this.getRepos()
  }

  /**
   * Saves given path in database. Treats path as unique.
   *
   * @param {object} payload - payload
   * @property {string} payload.path - path to save
   */
  save (payload) {
    // Try to retrieve that path from database
    const pathExists = db
      .get('repositories')
      .find({ path: payload.path })
      .size()
      .value()

    // Save path only if it not exist
    if (!pathExists) {
      const lastId = db.get('repositoriesLastId').value()

      db.get('repositories')
        .push({
          id: lastId + 1,
          path: payload.path,
        })
        .write()

      db.set('repositoriesLastId', lastId + 1).write()

      this.getRepos()
    }
  }
}

export default {
  state: initialState,
  klass: RepositoryManager,
}
