import Vue from 'vue'
import VStore from 'gitgit-vstore'
import Store from './store'
import App from './app'
import Prompt from './prompt'
import Repository from './repository'
import RepositoryManager from './repositoryManager'
import { cloneDeep } from 'lodash'

Vue.use(VStore)
VStore.init(Store.klass)
VStore.module(App.klass, 'app', cloneDeep(App.state))
VStore.module(Prompt.klass, 'prompt', cloneDeep(Prompt.state))
VStore.module(Repository.klass, 'repository', cloneDeep(Repository.state))
VStore.module(RepositoryManager.klass, 'repositoryManager', cloneDeep(RepositoryManager.state))
