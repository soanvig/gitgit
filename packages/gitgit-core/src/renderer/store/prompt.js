import VStore from 'gitgit-vstore'
import { ipcRenderer } from 'electron'

/**
 * Initial state, used for cloning into other objects.
 * Always clone.
 */
const initialState = {
  creds: {
    show: false,
    username: '',
    password: '',
    type: '',
  },
  newRef: {
    show: false,
    name: '',
  },
  upstream: {
    merge: null,
    remote: null,
    show: false,
  },
  pushUpstream: {
    show: false,
  },
}

class Prompt extends VStore {
  /**
   * Show prompt for credentials data.
   *
   * @param {object} payload - payload
   * @property {string} payload.username - username already retrieved
   * @property {string} payload.password - password already retrieved
   * @property {string} payload.type - 'http', 'agent', 'keyfile'
   */
  creds (payload) {
    if (payload.username && payload.password) {
      // Everything defined so quit
      this.state.creds.username = payload.username
      this.state.creds.password = payload.password
      this.credsFinish()

      return
    }

    this.state.creds.type = payload.type
    this.state.creds.show = true
    this.state.creds.username = payload.username || ''
  }

  /**
   * Cancels creds prompt.
   */
  credsCancel () {
    ipcRenderer.send('dynamic-answer-creds', {
      cancel: true,
    })

    this.state.creds.show = false
    this.state.creds.password = ''
    this.state.creds.type = ''
  }

  /**
   * Finish prompt for credentials data.
   */
  credsFinish () {
    ipcRenderer.send('dynamic-answer-creds', {
      username: this.state.creds.username,
      password: this.state.creds.password,
    })

    this.state.creds.show = false
    this.state.creds.password = ''
    this.state.creds.type = ''
  }

  /**
   * Cancels creds prompt.
   */
  cancelNewRef () {
    this.state.newRef.show = false
    this.state.newRef.name = ''
  }

  /**
   * Show prompt for new branch.
   */
  newRef () {
    this.state.newRef.show = true
    this.state.newRef.name = ''
  }

  /**
   * Sends new branch data to backend.
   */
  newRefFinish () {
    this.root.repository.git('newRef', this.state.newRef.name, true)
    this.state.newRef.show = false
  }

  /**
   * Ask if 'push.default upstream' should be set.
   */
  pushUpstream () {
    this.state.pushUpstream.show = true
  }

  /**
   * Finish pushUpstream prompt.
   *
   * @param {boolean} set - whether to set config entry or not or not
   */
  pushUpstreamFinish (set) {
    this.state.pushUpstream.show = false

    if (set) {
      this.root.repository.git('setPushUpstream')
    }
  }

  /**
   * Cancels upstream prompt.
   */
  upstreamCancel () {
    ipcRenderer.send('dynamic-answer-upstream', {
      cancel: true,
    })

    this.state.upstream.show = false
    this.state.upstream.password = ''
    this.state.upstream.type = ''
  }

  /**
   * Show prompt for upstream data.
   *
   * @param {object} payload - payload
   * @property {string} payload.remote - actual remote of branch
   * @property {string} payload.merge - actual merge of branch
   */
  upstream (payload) {
    this.state.upstream.merge = payload.merge
    this.state.upstream.remote = payload.remote
    this.state.upstream.show = true
  }

  /**
   * Finish upstream prompt.
   *
   * @param {object} payload - payload
   * @property {string} payload.remote - new remote of branch
   * @property {string} payload.merge - new merge of branch
   */
  upstreamFinish (payload) {
    ipcRenderer.send('dynamic-answer-upstream', {
      remote: payload.remote,
      merge: payload.merge,
    })

    this.state.upstream.merge = null
    this.state.upstream.remote = null
    this.state.upstream.show = false
  }
}

export default {
  state: initialState,
  klass: Prompt,
}
