import VStore from 'gitgit-vstore'

/**
 * Initial state, used for cloning into other objects.
 * Always clone.
 */
const initialState = {
  refsExplorerOpen: false,
}

class Prompt extends VStore {
}

export default {
  state: initialState,
  klass: Prompt,
}
