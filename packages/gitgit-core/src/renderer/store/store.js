import VStore from 'gitgit-vstore'
import { Notification } from 'element-ui'
import i18n from '@/i18n'

class Store extends VStore {
  notify (payload) {
    const notification = payload

    const args = notification.args || {}
    const message = notification.fullMessage || i18n.t(notification.message, args)
    const title = i18n.t(notification.title)
    Notification({
      ...notification,
      title,
      message,
    })
  }
}

export default {
  klass: Store,
}
