export default {
  computed: {
    /**
     * Returns all refs.
     * @returns {object}
     * @property {Array.<Reference>} head - local references
     * @property {Array.<Reference>} remote - remote references
     */
    refs () {
      return this.$store.repository.state.references
    },

    /**
     * Returns current branch.
     * @returns {Reference}
     */
    currentBranch () {
      return this.$store.repository.state.currentBranch
    },
  },
}
