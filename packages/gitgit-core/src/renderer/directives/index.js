import Vue from 'vue'

import vExternal from './v-external'

Vue.directive('external', vExternal)
