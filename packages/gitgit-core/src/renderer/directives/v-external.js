import { remote } from 'electron'
const shell = remote.shell

export default {
  bind (el, binding) {
    el.addEventListener('click', (e) => {
      e.preventDefault()
      const href = el.getAttribute('href')
      shell.openExternal(href)
    })
  },
}
