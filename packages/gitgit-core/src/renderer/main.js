import Vue from 'vue'
import VueLogger from 'vuejs-logger'
import './directives'

import ElementUI from 'element-ui'
import '@/assets/scss/element/index.css'
import locale from 'element-ui/lib/locale/lang/en'
import '@/assets/scss/ionicons.css'

import App from './views/App.vue'
import router from './router'
import i18n from './i18n'
import './store'

Vue.use(ElementUI, { locale, size: 'medium' })
Vue.use(VueLogger, {
  logLevel: 'debug',
})

Vue.config.productionTip = false
new Vue({
  router,
  i18n,
  render: h => h(App),
}).$mount('#app')
