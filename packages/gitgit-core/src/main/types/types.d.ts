type TCreds = 'http' | 'agent' | 'keyfile'

type TStatusFile = 'deleted' | 'modified' | 'created' | 'renamed'