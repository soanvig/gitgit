interface IUrlData {
  protocol: string
  username: string
  password: string
  host: string
  rootHost: string
}

interface ICreds {
  username?: string
  password?: string
  type?: TCreds
  unauthorized?: boolean
}

interface IUpstream {
  remote: string
  merge: string
}

interface IStatusFile {
  type: TStatusFile
  path: string
}

interface IStatusFiles {
  unstaged: Array<IStatusFile>
  staged: Array<IStatusFile>
}

interface IAheadBehind {
  ahead: number
  behind: number
}