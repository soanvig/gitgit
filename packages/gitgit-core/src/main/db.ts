import { app } from 'electron'
import * as Path from 'path'
import * as low from 'lowdb'
import * as fs from 'fs-extra'
import * as FileSync from 'lowdb/adapters/FileSync'

const userDataPath = app.getPath('userData')
const dbFilename = 'db.json'
const dbPath = Path.join(userDataPath, dbFilename)

if (!fs.existsSync(dbPath)) {
  console.log('Creating database')
  fs.ensureDirSync(userDataPath)
  fs.writeFileSync(dbPath, '')
}

const adapter = new FileSync(dbPath)
const db = low(adapter)

db.defaults({
  repositoriesLastId: 0,
  repositories: [],
}).write()

export default db
