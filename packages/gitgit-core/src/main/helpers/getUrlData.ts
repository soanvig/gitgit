/**
 * Matches URL against specific regexp:
 * e.g. http://username:password@host
 * And returs detailed URL description
 *
 * @param {string} url - url to match
 * @returns {object}
 * @property {string} protocol - protocol (http:// or https://)
 * @property {string} username - username
 * @property {string} password - password
 * @property {string} host - host
 * @property {string} rootHost - server address
 */

function getUrlData (url: string): IUrlData {
  const match = url.match(getUrlData.REGEXP)

  if (!match) {
    return null
  }

  return {
    protocol: match[1],
    username: match[4] || null,
    password: match[6] || null,
    host: match[7],
    rootHost: match[1] + '://' + match[7].split('/')[0],
  }
}

namespace getUrlData {
  export const REGEXP = /(http(s)?):\/\/(([^:]+)(:([^@]+))?@)?([^@]+)/
}

// Constant getUrlData regexp

export default getUrlData
