import * as nodegit from 'nodegit'
import { fullToShorthand } from './refName'

/**
 * Returns information about number of commits ahead and behind
 * Return { ahead: 0, behind: 0 } if some of refs does not exist
 * @param repo {Repository} - repository
 * @param refs {Map.<string, Reference>} - references
 * @param local {string} - shorthand or full name of reference
 * @param remote {string} - shorthand or full name of reference
 * @returns {IAheadBehind}
 */
export default async function getAheadBehind (
  repo: nodegit.Repository,
  refs: Map<string, nodegit.Reference>,
  local: string,
  remote: string,
): Promise<IAheadBehind> {

  // Ensure shorthand form
  const localRef: nodegit.Reference = refs.get(fullToShorthand(local))
  const remoteRef: nodegit.Reference = refs.get(fullToShorthand(remote))

  // If some is not defined, return zero diff
  if (!localRef || !remoteRef) {
    return { ahead: 0, behind: 0 }
  }

  // Get Oids of both refs
  const localOid = await nodegit.Reference.nameToId(repo, localRef.name())
  const remoteOid = await nodegit.Reference.nameToId(repo, remoteRef.name())

  // If some is not defined, return zero diff
  if (!localOid || !remoteOid) {
    return { ahead: 0, behind: 0 }
  }

  // @ts-ignore: it returns IAheadBehind, not number
  return nodegit.Graph.aheadBehind(repo, localOid, remoteOid)
}
