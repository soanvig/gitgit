/**
 * Returns shorthand form of full ref name:
 * refs/head/name -> name
 * refs/remotes/name -> name
 *
 * @param {string} name - name to convert to shorthand
 * @returns {string} - shorthand name
 */
export function fullToShorthand (name: string): string {
  return name.replace(/^refs\/heads\//, '').replace(/^refs\/remotes\//, '')
}

/**
 * Returns full form of shorthand name
 * name -> refs/head/name if !remote
 * name -> refs/remotes/name if remote
 *
 * @param {string} name - name to convert to full
 * @param {boolean} remote - determines if branch is remote or not
 * @returns {string} - full name
 */
export function shorthandToFull (name: string, remote: boolean = false): string {
  return remote ? `refs/remotes/${name}` : `refs/heads/${name}`
}

/**
 * Converts upstream ref name to local ref name targeting remote
 * remote:refs/heads/name -> remote/name
 *
 * @param {string} remote - name of remote (e.g. origin)
 * @param {string} name - full name of upstream ref (e.g. refs/heads/name)
 * @returns {string} - upstream name in local context (shorthand) (e.g. origin/name)
 */
export function upstreamToLocal (remote: string, name: string): string {
  name = fullToShorthand(name)
  return `${remote}/${name}`
}
