/**
 * Checks type of url
 *
 * @param {string} url - url to check
 * @returns {number} - -1 for unknown, 0 for ssh, 1 for http and 2 for https
 */
function getUrlType (url: string): UrlType {
  if (url.indexOf('https:') === 0) {
    return UrlType.HTTPS
  } else if (url.indexOf('http:') === 0) {
    return UrlType.HTTP
  } else if (
    url.indexOf('ssh:') === 0
    || (url.indexOf('@') > -1
        && url.indexOf(':') > url.indexOf('@'))
  ) {
    return UrlType.SSH
  } else {
    return UrlType.UNKNOWN
  }
}

/**
 * Enum for types of url
 *
 * @readonly
 * @enum {number}
 */
export enum UrlType {
  UNKNOWN = -1,
  SSH = 0,
  HTTP = 1,
  HTTPS = 2,
}

export default getUrlType
