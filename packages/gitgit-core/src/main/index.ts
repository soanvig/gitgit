import { app, BrowserWindow, protocol } from 'electron'
import db from './db'
import './git/index'
import startStatic from './static_server'
import * as fs from 'fs-extra'
import * as path from 'path'

export interface Global {
  db: any
}

declare const global: Global
global.db = db

let win: BrowserWindow

function createWindow () {
  try {
    fs.accessSync(path.join(__dirname, 'PROD'))

    // Create static server hosting Vue app
    console.log('Starting view.')
    startStatic()
  } catch (e) {
    console.log('Prodfile not existing. Entering dev mode.')
  }

  // Create the browser window.
  console.log('Creating window.')
  win = new BrowserWindow({
    width: 1024,
    height: 768,
    minWidth: 800,
    minHeight: 600,
    center: true,
    autoHideMenuBar: true,
  })

  console.log('Loading app')
  win.loadURL('http://localhost:9999')

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})
