import * as nodegit from 'nodegit'

export default class Config {
  public repo: nodegit.Repository = null
  public config: nodegit.Config = null

  constructor (repo: nodegit.Repository) {
    this.repo = repo
  }

  /**
   * Loads config into memory
   */
  async init () {
    this.config = await this.repo.config()
  }

  /**
   * Returns information if given repo has 'push.default' entry equal to 'upstream'
   *
   * @returns {boolean} - is push.default === 'upstream'
   */
  async isPushUpstream (): Promise<boolean> {
    try {
      const pushConfig: string = String(await this.config.getStringBuf('push.default'))
      return (pushConfig === 'upstream')
    } catch (e) {
      return false
    }
  }

  /**
   * Sets 'push.default upstream' in given repository
   */
  async setPushUpstream (): Promise<void> {
    await this.config.setString(`push.default`, 'upstream')
  }
}