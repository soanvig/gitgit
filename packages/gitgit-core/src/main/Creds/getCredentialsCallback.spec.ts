import * as nodegit from 'nodegit'
import * as ava from 'ava'
import getCredentialsCallback from './getCredentialsCallback'

const test = ava.test

test('should return function', (t) => {
  t.true(
    typeof getCredentialsCallback('_', '_', '_' as any) === 'function',
    'returned not a function',
  )
})

test('should return callback, which returns nodegit.Cred for each case', (t) => {
  let cb = getCredentialsCallback('_', '_', 'http')
  t.true(
    cb('_', '_') instanceof nodegit.Cred,
    'callback for http is not nodegit.Cred'
  )

  cb = getCredentialsCallback('_', '_', 'agent')
  t.true(
    cb('_', '_') instanceof nodegit.Cred,
    'callback for agent is not nodegit.Cred'
  )

  // Not implemented yet
  //
  // cb = getCredentialsCallback('_', '_', 'keyfile')
  // t.true(
  //   cb('_', '_') instanceof nodegit.Cred,
  //   'callback for keyfile is not nodegit.Cred'
  // )

  cb = getCredentialsCallback('_', '_', '_' as any, true)
  t.true(
    cb('_', '_') instanceof nodegit.Cred,
    'callback for unauthorized is not nodegit.Cred'
  )
})

test('should throw after some number of attempts for each case', (t) => {
  let cb = getCredentialsCallback('_', '_', 'http')

  t.throws(
    () => {
      for (let i = 0; i < 50; i += 1) {
        cb('_', '_')
      }
    },
    'error.http.credsInvalid',
    'http not threw or thew invalid error'
  )

  cb = getCredentialsCallback('_', '_', 'agent')
  t.throws(
    () => {
      for (let i = 0; i < 50; i += 1) {
        cb('_', '_')
      }
    },
    'error.ssh.agentInvalid',
    'agent not threw or thew invalid error'
  )

  // Not implemented yet
  //
  // cb = getCredentialsCallback('_', '_', 'keyfile')
  // t.throws(
  //   () => {
  //     for (let i = 0; i < 50; i += 1) {
  //       cb('_', '_')
  //     }
  //   },
  //   'error.ssh.keyfileInvalid',
  //   'keyfile not threw or thew invalid error'
  // )
})
