import * as nodegit from 'nodegit'

const MAX_ATTEMPTS: number = 5

export default function (
  username: string,
  password: string,
  type: TCreds,
  unauthorized?: boolean,
): Function {
  let attempts: number = 0

  return (url: string, internalUsername: string): nodegit.Cred => {
    attempts += 1

    // if too many attempts, throw
    if (attempts > MAX_ATTEMPTS) {
      let errorType: string

      switch (type) {
        case 'http': errorType = 'http.credsInvalid'; break
        case 'agent': errorType = 'ssh.agentInvalid'; break
        case 'keyfile': errorType = 'ssh.keyfileInvalid'; break
      }

      // reset counter
      attempts = 0

      throw new Error(`error.${errorType}`)
    }

    if (unauthorized) {
      return nodegit.Cred.defaultNew()
    }

    if (type === 'agent') {
      return nodegit.Cred.sshKeyFromAgent(internalUsername)
    } else if (type === 'keyfile') {
      // return node
    } else if (type === 'http') {
      return nodegit.Cred.userpassPlaintextNew(
        username,
        password,
      )
    }
  }
}
