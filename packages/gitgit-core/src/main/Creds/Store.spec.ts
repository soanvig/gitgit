import * as ava from 'ava'
import { setup, teardown } from '../../../tests/utils/setup'
import Store from './Store'
import * as path from 'path'

const test = ava.test

test.beforeEach('Setup', async (t: any) => {
  const directoryPath: string = await setup('Store')
  const storePath: string = path.join(directoryPath, '.git-credentials')

  t.context = {
    directoryPath,
    storePath,
    store: new Store(storePath),
  }
})

test.afterEach('Teardown', async (t: any) => {
  await teardown(t.context.directoryPath)
})

test('should save path in memory', (t) => {
  const store: Store = (t.context as any).store
  const storePath: string = (t.context as any).storePath

  t.is(store.path, storePath, 'store path is not equal given path')
})

test('should match given entry', (t) => {
  const store: Store = (t.context as any).store

  const onlyHost: IUrlData = {
    protocol: 'https',
    username: '',
    password: '',
    host: '',
    rootHost: 'https://gitlab.com',
  }

  t.deepEqual(
    store.findEntry(onlyHost),
    {
      protocol: 'https',
      username: 'username',
      password: 'password',
      host: 'gitlab.com',
      rootHost: 'https://gitlab.com',
    },
    'entry without (optional) username was not found'
  )

  const usernameHost: IUrlData = {
    protocol: 'https',
    username: 'username',
    password: '',
    host: '',
    rootHost: 'https://gitlab.com',
  }

  t.deepEqual(
    store.findEntry(onlyHost),
    {
      protocol: 'https',
      username: 'username',
      password: 'password',
      host: 'gitlab.com',
      rootHost: 'https://gitlab.com',
    },
    'entry with username was not found'
  )
})

test('should return null when no entry found', (t) => {
  const store: Store = (t.context as any).store

  const fakeData: IUrlData = {
    protocol: '',
    username: '',
    password: '',
    host: '',
    rootHost: '',
  }

  t.is(store.findEntry(fakeData), null, 'store returned data other than null')
})
