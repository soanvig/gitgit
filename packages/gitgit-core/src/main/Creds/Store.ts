import * as fs from 'fs'
import getUrlData from '../helpers/getUrlData'
import expandHomeDir from 'expand-home-dir'

class Store {
  public path: string
  private data: Array<string>

  constructor (path: string) {
    if (!path || !fs.existsSync(path)) {
      throw new Error(`Store "${path}" not found`)
    }

    // open store
    this.path = path
    this.data = fs.readFileSync(path, 'utf-8').split('\n')
  }

  findEntry (counterpart: IUrlData): IUrlData {
    let matchingEntry: IUrlData = null

    this.data.some((storeLine) => {
      // Retrieve URL information
      const storeLineData = getUrlData(storeLine)

      // If storeline is not defined, empty, whatever
      if (!storeLineData) {
        return false
      }

      if (
        // Optional username must match
        (!counterpart.username || counterpart.username === storeLineData.username)
        // root host must match
        && counterpart.rootHost === storeLineData.rootHost
      ) {
        // Result found, quit
        matchingEntry = storeLineData
        return true
      }
    })

    return matchingEntry
  }
}

/**
 * Returns git store path
 *
 * @param {string} credentialsHelper - git config credentials helper value
 * @returns {string} - path to git credentials store
 */
export function getStorePath (credentialsHelper: string): string {
  // It is store
  const fileArg = '--file'
  const storeFileIndex = credentialsHelper.indexOf(fileArg)
  const storeLocations = [
    '~/.git-credentials',
    '$XDG_CONFIG_HOME/git/credentials',
    null,
  ]

  if (storeFileIndex > -1) {
    // name exists, so add it to locations
    storeLocations.unshift(credentialsHelper.substr(storeFileIndex + fileArg.length + 1))
  }

  // Iterate over all possible locations until some is found
  let storeFilename
  let i = 0
  do {
    storeFilename = expandHomeDir(storeLocations[i])
    i += 1
  } while (!fs.existsSync(storeFilename) && i < storeLocations.length)

  return storeFilename
}

export default Store
