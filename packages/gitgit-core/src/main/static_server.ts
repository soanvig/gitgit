/**
 * Static server is used to host webpage in production
 * https://gist.github.com/ryanflorence/701407
 */

import * as http from 'http'
import * as url from 'url'
import * as path from 'path'
import * as fs from 'fs'

const port = '9999'

export default function (): void {
  http.createServer(function (request: any, response: any) {

    const uri = url.parse(request.url).pathname
    let filename = path.join(__dirname, 'renderer', uri)

    fs.exists(filename, function (exists: boolean) {
      if (!exists) {
        response.writeHead(404, { 'Content-Type': 'text/plain' })
        response.write('404 Not Found\n')
        response.end()
        return
      }

      if (fs.statSync(filename).isDirectory()) filename += '/index.html'

      fs.readFile(filename, 'binary', function (err: any, file: any) {
        if (err) {
          response.writeHead(500, { 'Content-Type': 'text/plain' })
          response.write(err + '\n')
          response.end()
          return
        }

        response.writeHead(200)
        response.write(file, 'binary')
        response.end()
      })
    })
  }).listen(parseInt(port, 10))

}
