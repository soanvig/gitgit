import * as nodegit from 'nodegit'
import { Git } from './Git'

declare module './Git' {
  interface Git {
    /**
     * Commits staged changes with supplied message.
     * Refreshes status.
     *
     * @async
     * @param {string} message - message for commit (title \n description # comment)
     */
    commit (this: Git, message: string): Promise<void>
  }
}

Git.prototype.commit = async function (message): Promise<void> {
  const repo = this.repository

  const index: nodegit.Index = await repo.refreshIndex()

  // Validate empty commit, and quit if such
  const status: Array<nodegit.StatusFile> = await repo.getStatus()
  const newFiles = status.filter((file) => file.inIndex())
  if (!newFiles.length) {
    return
  }

  const treeOid: nodegit.Oid = await index.writeTree()
  const allRefs: Array<nodegit.Reference> = await repo.getReferences(nodegit.Reference.TYPE.LISTALL)
  const headCommit: nodegit.Commit = await repo.getHeadCommit()
  const signature: nodegit.Signature = repo.defaultSignature()

  const commitId: nodegit.Oid = await repo.createCommit(
    'HEAD',
    signature,
    signature,
    message,
    treeOid,
    headCommit ? [headCommit] : []
  )

  this.answer('notify', {
    type: 'success',
    title: 'general.success',
    message: 'repository.commit.success',
    args: { commitId: commitId.tostrS() },
    duration: 1500,
  })

  await this.status()
}
