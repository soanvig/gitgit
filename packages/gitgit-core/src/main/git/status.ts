import { Git } from './Git'
import { Status } from 'nodegit'

declare module './Git' {
  interface Git {
    /**
     * Downloads status, then parses it, and saves files.
     *
     * @async
     */
    status (this: Git): Promise<void>
  }
}

Git.prototype.status = async function () {
  const status = await this.repository.getStatus()

  // Prepare array (which is exact as initial)
  const files: IStatusFiles = {
    unstaged: [],
    staged: [],
  }

  status.forEach((file) => {
    let type: TStatusFile
    let stage: 'unstaged' | 'staged'

    if (file.inWorkingTree()) {
      stage = 'unstaged'
    } else if (file.inIndex()) {
      stage = 'staged'
    } else {
      // unsupported file state
      return
    }

    if (file.isDeleted()) {
      type = 'deleted'
    } else if (file.isModified()) {
      type = 'modified'
    } else if (file.isNew()) {
      type = 'created'
    } else if (file.isRenamed()) {
      type = 'renamed'
    } else {
      // unsupported file type
      return
    }

    files[stage].push({
      type,
      path: file.path(),
    })
  })

  this.answer('saveFiles', files)
}
