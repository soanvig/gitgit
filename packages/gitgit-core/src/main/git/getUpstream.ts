import { Git } from './Git'
import * as nodegit from 'nodegit'
import { fullToShorthand, shorthandToFull } from '../helpers/refName';

declare module './Git' {
  interface Git {
    /**
     * Tries to retrieve upstream for given branch.
     * Requires upstream if not defined in config.
     *
     * @async
     * @param {string} branch - branch to get upstream of
     * @param {boolean} [ask=false] - determines whether it should ask view for password
     * @returns {Promise.<IUpstream>}
     */
    getUpstream (this: Git, branch: string, ask?: boolean): Promise<IUpstream>
  }
}

Git.prototype.getUpstream = async function (branch, ask = true): Promise<IUpstream> {
  const config = await this.repository.config()

  let remoteName: string
  let mergeName: string

  // Retrieve upstream remote name
  try {
    remoteName = String(await config.getStringBuf(`branch.${branch}.remote`))
  } catch (err) {
    remoteName = ''
  }

  // Retrieve upstream merge branch name
  try {
    mergeName = String(await config.getStringBuf(`branch.${branch}.merge`))
  } catch (err) {
    mergeName = ''
  }

  // If something is not set in upstream, prompt it
  if ((!remoteName || !mergeName) && ask) {
    this.answer('notify', {
      type: 'warning',
      title: 'general.warning',
      message: 'repository.push.upstream',
      duration: 1500,
    })

    const customUpstream = await new Promise<IUpstream>((resolve, reject) => {
      // Ask for upstream
      this.answer('prompt', 'upstream', {
        remote: remoteName,
        merge: fullToShorthand(mergeName),
      })

      // Wait for answer
      this.ipcMain.once(
        'dynamic-answer-upstream',
        (event: Electron.Event, data: {
          remote: string,
          merge: string,
          cancel?: boolean
        }) => {
          if (data.cancel) {
            reject('ERR_CANCEL')
            return
          }

          resolve(data)
        }
      )
    })

    remoteName = customUpstream.remote
    mergeName = shorthandToFull(customUpstream.merge)
    await config.setString(`branch.${branch}.remote`, customUpstream.remote)
    await config.setString(`branch.${branch}.merge`, mergeName)
  }

  return { remote: remoteName, merge: mergeName }
}
