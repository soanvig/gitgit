import * as nodegit from 'nodegit'
import { Git } from './Git'
import getAheadBehind from '../helpers/getAheadBehind'
import { upstreamToLocal } from '../helpers/refName'
import './getUpstream'

declare module './Git' {
  interface Git {
    /**
     * Loads available references.
     *
     * @async
     */
    loadReferences (this: Git): Promise<void>
  }
}

Git.prototype.loadReferences = async function (): Promise<void> {
  const repo = this.repository

  // Load all references
  const references: Array<nodegit.Reference> = await
    repo.getReferences(nodegit.Reference.TYPE.LISTALL)

  // Sort references to head and remote
  const head = references.filter((ref) => ref.isBranch())
  const remote = references.filter((ref) => ref.isRemote())

  // Save references for internal usage
  references.forEach((ref) => {
    this.refs.set(ref.shorthand(), ref)
  })

  // Create shorthands form to pass them to view
  const viewHead = await Promise.all(head.map(async (ref) => {
    const name: string = ref.shorthand()
    const upstream: IUpstream = await this.getUpstream(name, false)
    const aheadBehind: IAheadBehind = await getAheadBehind(
      this.repository,
      this.refs,
      ref.shorthand(),
      upstreamToLocal(upstream.remote, upstream.merge),
    )

    return {
      name,
      behind: aheadBehind.behind,
      ahead: aheadBehind.ahead,
    }
  }))

  const viewRemote = remote.map((ref) => ref.shorthand())

  this.answer('saveReferences', viewHead, viewRemote)

  const reference: nodegit.Reference = await repo.getCurrentBranch()

  this.answer('saveCurrentBranch', reference.shorthand())
}
