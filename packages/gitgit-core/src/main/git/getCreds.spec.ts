import * as sinon from 'sinon'
import nodegit from 'nodegit'
import './getCreds'
import { createTest } from '../../../tests/utils'
import { UrlType } from '../helpers/getUrlType'

const test = createTest('getCreds', ['status'])

// remote noUsername http://remote.com/
// remote username http://foobar@remote.com/
// remote usernamePassword http://foobar:barfoo@remote.com
// remote https https://remote.com
// remote ssh git@remote.com:repo.git

test.serial.beforeEach((t) => {
  t.context.git.answer = sinon.spy()
  t.context.git.ipcMain.once = (_: any, cb: any) => {
    cb(null, { username: 'foo', password: 'bar' })
  }
})

test('should work with HTTP', async (t) => {
  const { git, repoPath } = t.context

  await git.getCreds('noUsername')

  const args = (git.answer as sinon.SinonSpy).lastCall.args

  t.deepEqual(
    args,
    [
      'prompt',
      'creds',
      {
        username: null,
        password: null,
        type: UrlType.HTTP,
      },
    ],
    'Prompted incorrect values',
  )

})

test('should work with HTTP username', async (t) => {
  const { git, repoPath } = t.context

  await git.getCreds('username')

  const args = (git.answer as sinon.SinonSpy).lastCall.args

  t.deepEqual(
    args,
    [
      'prompt',
      'creds',
      {
        username: 'foobar',
        password: null,
        type: UrlType.HTTP,
      },
    ],
    'Prompted incorrect values',
  )

})

test('should work with HTTP username:password', async (t) => {
  const { git, repoPath } = t.context

  await git.getCreds('usernamePassword')

  const args = (git.answer as sinon.SinonSpy).lastCall.args

  t.deepEqual(
    args,
    [
      'prompt',
      'creds',
      {
        username: 'foobar',
        password: 'barfoo',
        type: UrlType.HTTP,
      },
    ],
    'Prompted incorrect values',
  )

})

test('should work with HTTPS', async (t) => {
  const { git, repoPath } = t.context

  await git.getCreds('https')

  const args = (git.answer as sinon.SinonSpy).lastCall.args

  t.deepEqual(
    args,
    [
      'prompt',
      'creds',
      {
        username: null,
        password: null,
        type: UrlType.HTTPS,
      },
    ],
    'returns incorrect result'
  )

})

test('should work with SSH', async (t) => {
  const { git, repoPath } = t.context

  const result = await git.getCreds('ssh')

  t.deepEqual(
    result,
    {
      type: 'agent',
    },
    'Prompted incorrect values',
  )

})

test('should send data request', async (t) => {
  const { git, repoPath } = t.context

  const result = await git.getCreds('noUsername')

  t.deepEqual(
    result,
    {
      username: 'foo',
      password: 'bar',
      type: 'http',
    },
    'Prompted incorrect values',
  )

})
