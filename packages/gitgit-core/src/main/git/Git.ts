import * as nodegit from 'nodegit'
import Config from '../Config/index'

export class Git {
  // Allow for calling actions via index
  [index: string]: any

  public answer: (action: string, ...args: any[]) => void

  public repository: nodegit.Repository

  public config: Config

  // Holds all references
  public refs: Map<string, nodegit.Reference> = new Map()

  async action (event: any, action: string, ...args: any[]) {
    this.answer = (action, ...args) => {
      event.sender.send('store', action, ...args)
    }

    if (!this[action]) {
      this.answer('notify', {
        type: 'error',
        title: 'general.error',
        message: 'error.git.noAction',
        args: { action },
        duration: 3500,
      })

      return
    }

    try {
      await this[action](...args)
    } catch (error) {
      // Cancel event propagates from functions and cancels action, so no error here
      if (error === 'ERR_CANCEL') {
        return
      }

      this.answer('notify', {
        type: 'error',
        title: 'general.error',
        fullMessage: error.message,
        duration: 3500,
      })
    }
  }

  /**
   * Initializes repository, executes other actions.
   *
   * @param {string} path - path to repo
   */
  async init (path: string): Promise<void> {
    const repo = await nodegit.Repository.open(path)

    this.repository = repo
    this.config = new Config(repo)

    await this.loadReferences()
    await this.status()
    await this.config.init()

    const isPushUpstream: boolean = await this.config.isPushUpstream()
    if (!isPushUpstream) {
      this.answer('prompt', 'pushUpstream')
    }
  }

  /**
   * Sets 'push.default' to 'upstream'
   */
  setPushUpstream (): void {
    this.config.setPushUpstream()
  }
}
