import { Git } from './Git'
import * as nodegit from 'nodegit'
import './getUpstream'
import './fetch'

declare module './Git' {
  interface Git {
    /**
     * Pulls selected branch from its upstream (fetch + merge)
     *
     * @async
     * @param {string} branch - branch to pull
     */
    pull (this: Git, branch: string): Promise<void>
  }
}

Git.prototype.pull = async function (branch): Promise<void> {
  // Get upstream
  const { remote, merge } = await this.getUpstream(branch)

  // get name in pattern: {remote}/{branch name}
  const mergeRef = `${remote}/${merge.replace('refs/heads/', '')}`
  const targetRef = await this.repository.getReference(branch)

  return this.fetch(remote, true)
    .then(() => {
      // @ts-ignore
      return this.repository.mergeBranches(targetRef, mergeRef)
    })
    .then(() => {
      this.answer('notify', {
        type: 'success',
        title: 'general.success',
        message: 'repository.pull.success',
        duration: 1500,
      })
    })
}
