import getCredentialsCallback from '../Creds/getCredentialsCallback'
import { Git } from './Git'

declare module './Git' {
  interface Git {
    /**
     * Fetches from selected remote
     *
     * @async
     * @param {string} remote - remote to fetch
     * @param {boolean} [noSuccess=false] - if true, then no success notification
     * will be displayed
     */
    fetch (this: Git, remote: string, noSuccess?: boolean): Promise<void>
  }
}

Git.prototype.fetch = async function (remote, noSuccess = false): Promise<void> {
  const { username, password, type, unauthorized } = await this.getCreds(remote)

  return this.repository
    .fetch(remote, {
      callbacks: {
        credentials: getCredentialsCallback(username, password, type, unauthorized),
        certificateCheck: () => 1,
      },
    })
    .then(() => {
      if (noSuccess) {
        return
      }

      this.answer('notify', {
        type: 'success',
        title: 'general.success',
        message: 'repository.fetch.success',
        duration: 1500,
      })
    })
}
