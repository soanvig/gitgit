import * as nodegit from 'nodegit'
import { Git } from './Git'
import { ipcMain } from 'electron'
import './add'
import './checkout'
import './commit'
import './loadReferences'
import './push'
import './remove'
import './status'
import './fetch'
import './pull'
import './getCreds'
import './getUpstream'
import './newRef'

// install ipcMain
Git.prototype.ipcMain = ipcMain

const git = new Git()

ipcMain.on('git', (event: Electron.Event, action: string, ...args: any[]) => {
  git.action(event, action, ...args)
})

export default git
