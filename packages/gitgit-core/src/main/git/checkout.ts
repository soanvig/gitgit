import { Git } from './Git'
import * as nodegit from 'nodegit'
import './status'

declare module './Git' {
  interface Git {
    /**
     * Checkouts branch.
     * Refreshes status.
     *
     * @async
     * @param {string} branch - repository's branch
     * @param {boolean} noSuccess - determines if notification should be displayed
     */
    checkout (this: Git, branch: string, noSuccess?: boolean): Promise<void>
  }
}

Git.prototype.checkout = async function (branch, noSuccess = false) {
  const ref = await this.repository.getReference(branch)

  return this.repository.checkoutRef(ref).then(() => {
    this.answer('saveCurrentBranch', branch)

    if (!noSuccess) {
      this.answer('notify', {
        type: 'success',
        title: 'general.success',
        message: 'repository.checkout.success',
        args: { ref: ref.shorthand() },
        duration: 1500,
      })
    }

    this.status()
  })
}
