import getUrlType, { UrlType } from '../helpers/getUrlType'
import getUrlData from '../helpers/getUrlData'
import Store, { getStorePath } from '../Creds/Store'
import { Git } from './Git'

declare module './Git' {
  interface Git {
    /**
     * Tries to retrieve credentials from any available source
     *
     * @param {string} remoteName - remote to getCreds to
     * @returns {Promise.<{ username: string, password: string, ssh: string }>}
     */
    getCreds (this: Git, remoteName: string): Promise<ICreds>
  }
}

Git.prototype.getCreds = async function (remoteName): Promise<ICreds> {
  const config = await this.repository.config()
  const remote = await this.repository.getRemote(remoteName)
  const remoteUrl = remote.url()
  const remoteUrlType = getUrlType(remoteUrl)

  let username: string
  let password: string
  let ssh = false

  if (remoteUrlType === UrlType.HTTP || remoteUrlType === UrlType.HTTPS) {
    // it's HTTP(S)!
    const credentialsHelper: string = String(await config.getStringBuf('credential.helper'))

    // Get remote URL information
    const remoteUrlData: IUrlData = getUrlData(remoteUrl)

    // Try to retrieve username and password from URL
    username = remoteUrlData.username
    password = remoteUrlData.password

    if (credentialsHelper.indexOf('store') === 0) {
      const storeFilename: string = getStorePath(credentialsHelper)
      let store: Store

      try {
        store = new Store(storeFilename)
      } catch (e) {
        console.error(e)
        return
      }

      // Find matching entry in store
      const matchingEntry: IUrlData = store.findEntry(remoteUrlData)

      // Try to define user from remote (favor already defined)
      username = username || matchingEntry.username

      if (matchingEntry) {
        // If any store matching entry was found match password (favor already defined)
        password = password || matchingEntry.password
      }
    } else if (credentialsHelper.indexOf('cache') === 0) {
      // It is cache
      // We don't support cache yet :(
      // Ask for creds
    } else {
      // It is nothing, ask for credentials
    }
  } else if (remoteUrlType === UrlType.SSH) {
    ssh = true
  } else {
    // Unknown remote type
    this.answer('notify', {
      type: 'error',
      title: 'general.error',
      message: 'repository.creds.unknownRemoteType',
      duration: 3500,
    })

    return
  }

  // if username or password was not defined, show prompt
  if ((!username || !password) && !ssh) {
    this.answer('notify', {
      type: 'warning',
      title: 'general.warning',
      message: 'repository.creds.info',
      duration: 3500,
    })
  }

  if (ssh) {
    return { type: 'agent' }
  }

  return new Promise<ICreds>((resolve, reject) => {
    // Decode data (because e.g. in password @ may be saved as %40)
    username = username ? decodeURIComponent(username) : null
    password = password ? decodeURIComponent(password) : null

    // Ask for creds
    this.answer('prompt', 'creds', { username, password, type: remoteUrlType })

    // Wait for answer
    this.ipcMain.once(
      'dynamic-answer-creds',
      (event: Electron.Event, data: { username: string, password: string, cancel?: boolean }) => {
        if (data.cancel) {
          reject('ERR_CANCEL')
          return
        }

        resolve({
          username: data.username,
          password: data.password,
          type: 'http',
        })
      }
    )
  })
}
