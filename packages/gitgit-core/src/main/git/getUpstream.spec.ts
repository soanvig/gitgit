import * as sinon from 'sinon'
import nodegit from 'nodegit'
import './getUpstream'
import { createTest } from '../../../tests/utils'
import { UrlType } from '../helpers/getUrlType'

const test = createTest('getUpstream', [])

test.serial.beforeEach((t) => {
  t.context.git.answer = sinon.spy()
  t.context.git.ipcMain.once = (_: any, cb: any) => {
    cb(null, { remote: 'foo', merge: 'bar' })
  }
})

test('should retrieve remote and merge from branch config', async (t) => {
  const { git } = t.context

  const result = await git.getUpstream('remoteMerge')

  t.deepEqual(
    result,
    { remote: 'foo', merge: 'refs/heads/bar' },
    'Returned incorrect values',
  )
  t.true((git.answer as sinon.SinonSpy).notCalled, 'Answer was called')
})

test('should retrieve remote and asks for merge', async (t) => {
  const { git } = t.context

  const result = await git.getUpstream('remote')
  const args = (git.answer as sinon.SinonSpy).lastCall.args

  t.deepEqual(
    args,
    ['prompt', 'upstream', { remote: 'foo', merge: '' }],
    'Requested with incorrect values'
  )
  t.deepEqual(result, { remote: 'foo', merge: 'refs/heads/bar' }, 'Returned incorrect values')
})

test('should retrieve merge and asks for remote', async (t) => {
  const { git } = t.context

  const result = await git.getUpstream('merge')
  const args = (git.answer as sinon.SinonSpy).lastCall.args

  t.deepEqual(
    args,
    ['prompt', 'upstream', { remote: '', merge: 'bar' }],
    'Requested with incorrect values'
  )
  t.deepEqual(result, { remote: 'foo', merge: 'refs/heads/bar' }, 'Returned incorrect values')
})

test('should ask for remote and merge', async (t) => {
  const { git } = t.context

  const result = await git.getUpstream('none')
  const args = (git.answer as sinon.SinonSpy).lastCall.args

  t.deepEqual(
    args,
    ['prompt', 'upstream', { remote: '', merge: '' }],
    'Requested with incorrect values',
  )
  t.deepEqual(result, { remote: 'foo', merge: 'refs/heads/bar' }, 'Returned incorrect values')
})

test('should save given values in config', async (t) => {
  const { git } = t.context
  const config = await git.repository.config()

  const result = await git.getUpstream('none')

  const remote = String(await config.getStringBuf(`branch.none.remote`))
  const merge = String(await config.getStringBuf(`branch.none.merge`))

  t.is(remote, 'foo', 'Did not saved remote properly')
  t.is(merge, 'refs/heads/bar', 'Did not saved merge properly')
})
