import * as sinon from 'sinon'
import nodegit from 'nodegit'
import './status'
import { createTest } from '../../../tests/utils'

const test = createTest('status', ['answer'])

test('should properly detect unstaged files', async (t) => {
  const { git, repoPath } = t.context

  await git.status()

  const args: any[] = (git.answer as sinon.SinonSpy).lastCall.args

  const unstaged = args[1].unstaged.sort((a: any, b: any) => a.type < b.type ? -1 : 1)

  t.deepEqual(
    unstaged,
    [
      {
        type: 'created',
        path: 'added-unstaged',
      },
      {
        type: 'deleted',
        path: 'removed-unstaged',
      },
      {
        type: 'modified',
        path: 'modified-unstaged',
      },
    ],
    'Unstaged files do not meet expected'
  )
})

test('should properly detect staged files', async (t) => {
  const { git, repoPath } = t.context

  await git.status()

  const args: any[] = (git.answer as sinon.SinonSpy).lastCall.args

  const staged = args[1].staged.sort((a: any, b: any) => a.type < b.type ? -1 : 1)

  t.deepEqual(
    staged,
    [
      {
        type: 'created',
        path: 'added-staged',
      },
      {
        type: 'deleted',
        path: 'removed-staged',
      },
      {
        type: 'modified',
        path: 'modified-staged',
      },
    ],
    'Staged files do not meet expected'
  )
})

test('should call saveFiles answer', async (t) => {
  const { git, repoPath } = t.context

  await git.status()

  const args: any[] = (git.answer as sinon.SinonSpy).lastCall.args

  const answerName = args[0]

  t.is(answerName, 'saveFiles', 'Answer did not called `saveFiles`')
})
