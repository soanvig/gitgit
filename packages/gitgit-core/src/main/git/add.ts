import * as nodegit from 'nodegit'
import { Git } from './Git'

declare module './Git' {
  interface Git {
    /**
     * Adds files to index.
     * Refreshes status.
     *
     * @async
     * @param {Array.<string> | string} files - paths to add
     */
    add (this: Git, files: Array<string> | string): Promise<void>
  }
}

Git.prototype.add = async function (files): Promise<void> {
  // ensure 'files' is an array
  files = [].concat(files)

  const index = await this.repository.refreshIndex()

  return index
    .addAll(files, 0)
    .then(() => index.write())
    .then(() => index.writeTree())
    .then(() => this.status())
    .then(() => {
      this.answer('notify', {
        type: 'success',
        title: 'general.success',
        message: 'repository.add.success',
        duration: 1500,
      })
    })
}
