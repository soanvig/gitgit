import * as nodegit from 'nodegit'
import { Git } from './Git'

declare module './Git' {
  interface Git {
    /**
     * Removes files from index.
     * Refreshes status.
     *
     * @async
     * @param {Array.<string>} files - paths to remove
     */
    remove (this: Git, files: Array<string>): Promise<void>
  }
}

Git.prototype.remove = async function (files) {
  const repo = this.repository

  // ensure files is an array
  files = [].concat(files)

  const headCommit = await repo.getHeadCommit()

  const index: nodegit.Index = await repo.refreshIndex()

  // @types/nodegit seems to have wrong type (Object instead of Commit)
  await nodegit.Reset.default(repo, headCommit as any, files)

  await index.write()
  await index.writeTree()

  await this.answer('notify', {
    type: 'success',
    title: 'general.success',
    message: 'repository.remove.success',
    duration: 1500,
  })
}
