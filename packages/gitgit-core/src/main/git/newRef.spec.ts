import * as sinon from 'sinon'
import nodegit from 'nodegit'
import './newRef'
import { createTest } from '../../../tests/utils'

const test = createTest('newRef', ['status', 'answer'])

test('should create reference of given name', async (t) => {
  const { git } = t.context

  await git.newRef('newRef')

  const allRefs: Array<string> = (
    await git.repository.getReferences(nodegit.Reference.TYPE.LISTALL)
  ).map((ref) => ref.shorthand())

  t.true(allRefs.indexOf('newRef') > -1, 'Did not created new ref')

})

test('should checkout new branch if ordered to do so', async (t) => {
  const { git } = t.context

  await git.newRef('newRef', true)

  const currentBranch: string = (await git.repository.getCurrentBranch()).shorthand()

  t.is(currentBranch, 'newRef', 'Did not checkouted to new branch')
})

test('should not checkout by default', async (t) => {
  const { git } = t.context

  await git.newRef('newRef')

  const currentBranch: string = (await git.repository.getCurrentBranch()).shorthand()

  t.not(currentBranch, 'newRef', 'Checkouted to new branch')
})

test('should return newly created reference', async (t) => {
  const { git } = t.context

  const newRef: nodegit.Reference = await git.newRef('newRef', true)

  t.is(newRef.shorthand(), 'newRef', 'Did not returned new reference')
})
