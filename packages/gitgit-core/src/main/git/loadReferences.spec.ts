import * as sinon from 'sinon'
import nodegit from 'nodegit'
import './loadReferences'
import { createTest } from '../../../tests/utils'

const test = createTest('loadReferences', [])

test.serial.beforeEach((t) => {
  t.context.git.answer = sinon.spy()
})

test('should load references into internal memory', async (t) => {
  const { git } = t.context

  await git.loadReferences()

  t.deepEqual(
    Array.from(git.refs.keys()).sort(),
    [
      'local1',
      'local2',
      'origin/remote1'
    ],
    'Returned incorrect references'
  )
})

test('should answer with separated local and remote branches', async (t) => {
  const { git } = t.context

  await git.loadReferences()

  const args = (git.answer as sinon.SinonSpy).firstCall.args
  t.deepEqual(
    [
      args[0],
      args[1].sort((a: any, b: any) => a.name < b.name ? -1 : 1),
      args[2].sort(),
    ],
    [
      'saveReferences',
      [
        {
          name: 'local1',
          behind: 0,
          ahead: 0,
        },
        {
          name: 'local2',
          behind: 0,
          ahead: 0,
        }
      ],
      [
        'origin/remote1'
      ],
    ],
    'Returned incorrect results'
  )
})

test('should answer with current branch', async (t) => {
  const { git } = t.context

  await git.loadReferences()

  t.deepEqual(
    (git.answer as sinon.SinonSpy).lastCall.args,
    [
      'saveCurrentBranch',
      'local2',
    ],
    'Returned incorrect references'
  )
})
