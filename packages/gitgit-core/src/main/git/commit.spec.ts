import * as sinon from 'sinon'
import nodegit from 'nodegit'
import './commit'
import { createTest, createFile } from '../../../tests/utils'

const test = createTest('commit', ['status', 'answer'])

test('should create commit in empty repo', async (t) => {
  const { git } = t.context

  await git.commit('Foobar')

  const headCommit: nodegit.Commit = await git.repository.getHeadCommit()
  const tree: nodegit.Tree = await headCommit.getTree()
  const treeEntries: Array<string> = tree.entries().map((e) => e.name())

  t.deepEqual(treeEntries, ['file1'], 'Tree contains file1')
})

test('should create commit with file in non-empty repo', async (t) => {
  const { git, repoPath } = t.context

  // Create initial commit
  await git.commit('Foobar')

  // Create and stage new file
  const newFile: string = await createFile(repoPath, 'file2')
  const index: nodegit.Index = await git.repository.refreshIndex()
  await index.addAll(['file2'], 0)
  await index.write()
  await index.writeTree()

  // Create second commit
  await git.commit('Foobar')

  const headCommit: nodegit.Commit = await git.repository.getHeadCommit()

  const history: any = headCommit.history()

  let commitsCount: number = 0

  await new Promise((resolve) => {
    history.on('commit', (commit: nodegit.Commit) => {
      commitsCount += 1
    })

    history.on('end', () => {
      t.is(commitsCount, 2, 'Not all commits were created')
      resolve()
    })

    history.start()
  })
})

test('should create commit with given message', async (t) => {
  const { git } = t.context

  await git.commit('Foobar')

  const headCommit: nodegit.Commit = await git.repository.getHeadCommit()

  t.is(headCommit.message(), 'Foobar', 'Commit was created with incorrect message')
})

test('should not create empty commit', async (t) => {
  const { git, repoPath } = t.context

  // Create initial commit
  await git.commit('Foobar1')

  // Immediately create second commit
  await git.commit('Foobar2')

  const headCommit: nodegit.Commit = await git.repository.getHeadCommit()

  t.is(
    headCommit.message(),
    'Foobar1',
    'First commit is not HEAD. Another commit was created afterwards.',
  )
})

test('should call notification', async (t) => {
  const { git } = t.context

  await git.commit('Foobar')

  t.true((git.answer as sinon.SinonSpy).calledWith('notify'), 'Answer notify was not called')
})

test('should call status', async (t) => {
  const { git } = t.context

  await git.commit('Foobar')

  t.is((git.status as sinon.SinonSpy).callCount, 1, 'Status was called zero or more than once')
})
