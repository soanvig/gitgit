import * as sinon from 'sinon'
import { createTest, someTime } from '../../../tests/utils'
import nodegit from 'nodegit'
import './fetch'

const test = createTest('fetch', ['answer'])

test('should download new commit from remote repository, and notify', async (t) => {
  const { git, repoPath } = t.context
  git.getCreds = async () => { return { unauthorized: true } }

  await git.fetch('origin')
  await someTime()

  const headCommit: nodegit.Commit =
    await git.repository.getReferenceCommit('refs/remotes/origin/master')

  t.is(
    headCommit.message().trim(),
    'Remote commit',
    'HEAD commit from remote was not downloaded.',
  )

  t.true((git.answer as sinon.SinonSpy).calledWith('notify'), 'Answer notify was not called')

})

test('should not notify if noSuccess', async (t) => {
  const { git, repoPath } = t.context
  git.getCreds = async () => { return { unauthorized: true } }

  await git.fetch('origin', true)
  await someTime()

  t.false((git.answer as sinon.SinonSpy).calledWith('notify'), 'Answer notify called')
})
