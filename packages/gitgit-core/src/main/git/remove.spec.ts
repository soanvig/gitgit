import * as sinon from 'sinon'
import nodegit from 'nodegit'
import './remove'
import { createTest } from '../../../tests/utils'

const test = createTest('remove', ['status'])

test('should remove files from index', async (t) => {
  const { git, repoPath } = t.context

  // Add files
  const filenames: Array<string> = ['file1', 'file2', 'file3']

  let index: nodegit.Index = await git.repository.refreshIndex()

  await index.addAll(filenames, 0)
  await index.write()
  await index.writeTree()

  // Try to remove files
  await git.remove(['file1', 'file2'])

  // Find files
  index = await git.repository.refreshIndex()
  const indexEntriesPaths: Array<string> = index.entries().map((e) => e.path)

  t.deepEqual(indexEntriesPaths, ['file3'], 'Index paths do not match real paths')
})
