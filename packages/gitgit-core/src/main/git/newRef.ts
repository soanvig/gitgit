import { Git } from './Git'
import * as nodegit from 'nodegit'
import './loadReferences'
import './checkout'

declare module './Git' {
  interface Git {
    /**
     * Creates new reference from current reference
     *
     * @async
     * @param name {string} - name of new reference
     * @param checkout {boolean} - determines if checkout to newly
     *  created branch should be performed
     * @returns {Reference} - newly created reference
     */
    newRef (this: Git, name: string, checkout?: boolean): Promise<nodegit.Reference>
  }
}

Git.prototype.newRef = async function (name, checkout = false): Promise<nodegit.Reference> {
  const repo = this.repository

  // Get Oid of HEAD
  const oid: nodegit.Oid = await nodegit.Reference.nameToId(repo, 'HEAD')

  // Get commit of Oid
  const commit: nodegit.Commit = await nodegit.Commit.lookup(repo, oid)

  // Create branch
  const branch: nodegit.Reference = await nodegit.Branch.create(repo, name, commit, 0)

  this.answer('notify', {
    type: 'success',
    title: 'general.success',
    message: 'repository.branch.createSuccess',
    args: { branch: name },
    duration: 1500,
  })

  // Refresh references
  await this.loadReferences()

  if (checkout) {
    await this.checkout(name, true)
  }

  return branch
}
