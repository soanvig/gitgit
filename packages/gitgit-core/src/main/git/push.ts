import getCredentialsCallback from '../Creds/getCredentialsCallback'
import { Git } from './Git'
import getAheadBehind from '../helpers/getAheadBehind'
import { upstreamToLocal } from '../helpers/refName'
import './getUpstream'
import './getCreds'

declare module './Git' {
  interface Git {
    /**
     * Pushes commits to specified remote and branch.
     *
     * @async
     * @param {string} branch - branch name
     */
    push (this: Git, branch: string): Promise<void>
  }
}

Git.prototype.push = async function (branch): Promise<void> {
  // Get upstream
  const { remote, merge } = await this.getUpstream(branch)

  const aheadBehind: IAheadBehind = await getAheadBehind(
    this.repository,
    this.refs,
    branch,
    upstreamToLocal(remote, merge),
  )

  // See if there are any commits ahead of remote
  if (aheadBehind.ahead === 0) {
    this.answer('notify', {
      type: 'success',
      title: 'general.success',
      message: 'repository.push.empty',
      duration: 3000,
    })

    return
  }

  // Get creds
  const { username, password, type } = await this.getCreds(remote)

  const localRef = await this.repository.getReference(branch)

  return this.repository.getRemote(remote)
    .then((remote) => {
      const upstreamRef = merge

      return remote.push(
        [`${localRef}:${upstreamRef}`],
        {
          callbacks: {
            credentials: getCredentialsCallback(username, password, type),
          },
          certificateCheck: () => 1,
        },
      )
    })
    .then(() => {
      this.answer('notify', {
        type: 'success',
        title: 'general.success',
        message: 'repository.push.success',
        args: {
          branch: `${remote}/${merge.replace('refs/heads/', '')}`,
        },
        duration: 1500,
      })
    })
}
