import * as sinon from 'sinon'
import nodegit from 'nodegit'
import './add'
import { createTest } from '../../../tests/utils'

const test = createTest('add', ['status'])

test('should add files to index', async (t) => {
  const { git, repoPath } = t.context

  const filenames: Array<string> = ['file1', 'file2', 'file3']

  await git.add(filenames)
  const index: nodegit.Index = await git.repository.refreshIndex()
  const indexEntriesPaths: Array<string> = index.entries().map((e) => e.path)

  t.deepEqual(indexEntriesPaths, filenames, 'Index paths do not match real paths')
})

test('should add files partially to index', async (t) => {
  const { git, repoPath } = t.context

  const filenames: Array<string> = ['file1', 'file2']

  await git.add(filenames)
  const index: nodegit.Index = await git.repository.refreshIndex()
  const indexEntriesPaths: Array<string> = index.entries().map((e) => e.path)

  t.deepEqual(indexEntriesPaths, filenames, 'Index paths do not match real paths')
})

test('should call status', async (t) => {
  const { git } = t.context

  await git.add([])

  t.is((git.status as sinon.SinonSpy).callCount, 1, 'Status was called zero or more than once')
})
