import * as sinon from 'sinon'
import { createTest } from '../../../tests/utils'
import nodegit from 'nodegit'
import './checkout'

const test = createTest('checkout', ['status', 'answer'])

test('should checkout to given branch name', async (t) => {
  const { git } = t.context

  await git.checkout('new-branch')

  const currentBranch = (await git.repository.getCurrentBranch()).shorthand()

  t.is(currentBranch, 'new-branch', 'Current branch is not new-branch')
})

test('should call status', async (t) => {
  const { git } = t.context

  await git.checkout('master')

  t.is((git.status as sinon.SinonSpy).callCount, 1, 'Status was called zero or more than once')
})

test('should call answer by default', async (t) => {
  const { git } = t.context

  await git.checkout('master')

  t.true((git.answer as sinon.SinonSpy).calledWith('notify'), 'Answer notify was not called')
})

test('should not call answer if disabled', async (t) => {
  const { git } = t.context

  await git.checkout('master', true)

  t.false((git.answer as sinon.SinonSpy).calledWith('notify'), 'Answer notify was called')
})
