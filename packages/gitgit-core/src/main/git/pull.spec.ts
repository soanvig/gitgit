import * as sinon from 'sinon'
import { createTest, someTime } from '../../../tests/utils'
import nodegit from 'nodegit'
import './pull'

const test = createTest('pull', ['answer'])

test('should merge changes from remote to local branch, and notify', async (t) => {
  const { git, repoPath } = t.context
  git.getCreds = async () => { return { unauthorized: true } }

  await git.pull('master')
  await someTime()

  const headCommit: nodegit.Commit = await git.repository.getHeadCommit()

  t.is(headCommit.message().trim(), 'newcommit', 'Head commit does not match remote commit')

  t.true((git.answer as sinon.SinonSpy).calledWith('notify'), 'Answer notify was not called')
})
