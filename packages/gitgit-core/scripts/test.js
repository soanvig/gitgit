(async () => {
  const server = require('gitgit-server')
  const cmd = require('commander')
  const shell = require('shelljs')
  const path = require('path')
  const unzip = require('extract-zip')
  const fs = require('fs-extra')

  const originalCwd = process.cwd()
  process.chdir(path.join(__dirname, '..'))

  cmd
    .option('-f, --files <items>', 'Test only given files')
    .parse(process.argv)

  // Start up git server
  const remoteRepoPath = path.join(process.cwd(), 'tests', 'temp', 'remotes')
  const remoteZipPath = path.join(process.cwd(), 'tests', 'envs', 'remotes.zip')

  // Unzip remotes
  await new Promise((resolve, reject) => {
    fs.ensureDir(remoteRepoPath)

    unzip(remoteZipPath, {
      dir: remoteRepoPath, defaultDirMode: 511, defaultFileMode: 511
    }, function (err) {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })

  // Await 1 sec
  await new Promise((resolve) => {
    setTimeout(() => {
      resolve()
    }, 1000)
  })

  server.start(remoteRepoPath)
  console.log('Git server started')

  let command = [
    'FORCE_COLOR=t',
    './node_modules/.bin/ava-ts',
    cmd.files ? cmd.files : 'src/main/**/*.spec.ts',
  ]
  console.log(command.join(' '))
  shell.exec(command.join(' '))

  server.stop()
  console.log('Git server stopped')

  // Cleanup any trash
  shell.exec('rm -rf tests/temp')
  console.log('Temp cleaned up')

  process.chdir(originalCwd)

  process.exit()
})()

