const fs = require('fs-extra')
const path = require('path')
const shell = require('shelljs')

const originalCwd = process.cwd()
process.chdir(path.join(__dirname, '..'))

fs.ensureDirSync('build')
fs.emptyDirSync('build')
shell.exec('touch build/PROD')

process.chdir(originalCwd)
