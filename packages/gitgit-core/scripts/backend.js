const shell = require('shelljs')
const path = require('path')
const cmd = require('commander')

cmd
  .option('--dist', 'Builds in distribution mode')
  .parse(process.argv)

const originalCwd = process.cwd()
process.chdir(path.join(__dirname, '..'))

if (!cmd.dist) {
  console.log('Building app from tsconfig.dev.json...')
  shell.exec('./node_modules/.bin/tsc -p ./tsconfig.dev.json')

  console.log('Launching electron...')
  shell.exec('./node_modules/.bin/electron tmp/index.js')
} else {
  console.log('Building app from tsconfig.prod.json...')
  shell.exec('./node_modules/.bin/tsc -p ./tsconfig.prod.json')
}

process.chdir(originalCwd)
