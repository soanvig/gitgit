# GitGit Core

GitGit Core is core app of GitGit stack. This is the app itself user uses. It is built upon Electron and contains both main and renderer. By this, it means: backend and frontend, or: git system and app view.

## Git system

Git system uses nodegit as base of Git support.

## App view

App view is built upon Vue framework.

## Setup

1. `yarn backend`
2. `yarn frontend`

## Build for distribution

GitGit uses `electron-builder`, and that has its configuration in `package.json` file.

```
yarn dist
```